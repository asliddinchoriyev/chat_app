import React, {Component} from 'react';
import './indexContent.css'

class Content extends Component {

    state = {
        inputValue: ''
    }

    changeMessageInput = (event) => {
        this.setState({
            inputValue: event.target.value
        })
    }

    sendMessage = () => {
        let fromId = 1
        let toId = this.props.selectedUser.id
        let text = this.state.inputValue
        this.props.sendMessage(fromId, toId, text)
        this.setState({
            inputValue: ''
        })
    }

    render() {

        const {selectedUser, history} = this.props
        const {inputValue} = this.state
        return (
            <div>
                {
                    <div className={'content'}>
                        <div className="row">
                            <div className="col-md-12 content-header">
                                {selectedUser === '' ?
                                    <h2 className={'text-center'}>Welcome our Chat group</h2>
                                    : <div>
                                        <h4 className={'ml-2 mt-2'}>{selectedUser.id + ". " + selectedUser.firstName + " " + selectedUser.lastName}</h4>
                                        <h6 className={'text-right'}>{selectedUser.phone}</h6>
                                    </div>
                                }
                            </div>
                            <div className="col-md-12 p-0 my-2">
                                <input
                                    type="text"
                                    className={'form-control'}
                                    value={inputValue}
                                    onChange={this.changeMessageInput}
                                    placeholder={'Message'}
                                />
                                <button className={'btn btn-success float-right px-3 my-2'}
                                        onClick={this.sendMessage}>Send
                                </button>
                            </div>
                            <div className="col-md-12 bg-white" style={{minHeight: '400px'}}>

                                {
                                    history.map((item, index) =>
                                        <div key={index} className={'row'}>
                                            {item.from === 1 ?
                                                <div className="col-md-10 message bg-success">
                                                    <p>
                                                        {item.text}
                                                        <span className={'float-right date'}>{item.date}</span>
                                                    </p>
                                                </div> :
                                                <div className="col-md-10 message bg-info"
                                                     style={{marginLeft: '150px'}}>
                                                    <p>
                                                        {item.text}
                                                        <span className={'float-right date'}>{item.date}</span>
                                                    </p>
                                                </div>
                                            }

                                        </div>)
                                }
                            </div>
                        </div>
                    </div>
                }
            </div>

        );
    }

}

export default Content;