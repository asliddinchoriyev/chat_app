import React, {Component} from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap'
import './indexSider.css'

class Sider extends Component {

    state = {
        modalVisible: false
    }

    modalToggle = () => {
        this.setState({
            modalVisible: !this.state.modalVisible
        })
    }

    onSubmit = (event) => {
        event.preventDefault();
        let firstName = event.target[0].value
        let lastName = event.target[1].value
        let phone = event.target[2].value
        this.props.addUser(firstName, lastName, phone)
        this.modalToggle()
    }

    userClicked = (user) => {
        this.props.selectUser(user)
    }

    render() {
        const {users, selectUser} = this.props
        const {modalVisible} = this.state
        return (
            <div className={'sider'}>
                <button className={'btn btn-dark btn-block mt-2'} onClick={this.modalToggle}>Add user</button>
                <hr/>

                <ul className={'list-group'}>
                    {
                        users.map((user, index) => (
                            <li onClick={() => this.userClicked(user)}
                                className={'list-group-item user'}
                                key={index}>{user.firstName + " " + user.lastName}</li>))
                    }
                < /ul>

                <Modal isOpen={modalVisible} toggle={this.modalToggle}>
                    <ModalHeader>
                        <h2>Add user</h2>
                    </ModalHeader>
                    <ModalBody>
                        <form onSubmit={this.onSubmit} id={'addUser'}>
                            FirstName<input required='required' className={'form-control'} type="text"/>
                            LastName<input required='required' className={'form-control'} type="text"/>
                            Phone<input required='required' className={'form-control'} type="text"/>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        <button className={'btn btn-success'} form={'addUser'}>Save</button>
                        <button className={'btn btn-danger'} onClick={this.modalToggle}>Close</button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Sider;