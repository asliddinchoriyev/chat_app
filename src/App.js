import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import Sider from './components/sider/Sider'
import Content from './components/content/Content'

import './index.css'

class App extends Component {

    state = {
        users: [],
        selectedUser: {
            id: 'Hello..',
            firstName: 'Welcome to',
            lastName: 'Our Chat Group',
            phone: ''
        },
        messages: [],
        history: []
    }

    addUser = (firstName, lastName, phone) => {
        let a = this.state.users
        a.push({id: a.length + 1, firstName: firstName, lastName: lastName, phone: phone})

        this.setState({
            users: a
        })

        localStorage.setItem('users', JSON.stringify(a))
    }

    selectUser = (user) => {
        this.setState({
            selectedUser: user
        })

        localStorage.setItem('selectedUser', JSON.stringify(user))
        this.getMessageHistory(user)
    }

    getMessageHistory = (user) => {
        let x = localStorage.getItem('messages')
        if (x) {
            let mesArray = JSON.parse(x)
            let history = mesArray.filter((message) => message.from === 1 && message.to === user.id || message.from === user.id && message.to === 1)
            this.setState({
                history: history
            })
        }

    }

    sendMessage = (fromId, toId, text) => {
        let date = new Date()
        let hours = (date.getHours().toString().length < 2 ? "0" + date.getHours().toString() : date.getHours())
        let minutes = (date.getMinutes().toString().length < 2 ? "0" + date.getMinutes().toString() : date.getMinutes())
        let message = {
            from: fromId,
            to: toId,
            text: text,
            date: hours + ":" + minutes
        }

        let a = this.state.messages
        a.push(message);
        this.setState({
            messages: a
        })

        localStorage.setItem('messages', JSON.stringify(a))

        this.getMessageHistory(this.state.users.filter((item) => item.id === toId)[0])
    }

    componentDidMount() {
        let usersString = localStorage.getItem('users')
        if (usersString) {
            let usersArray = JSON.parse(usersString);
            this.setState({
                users: usersArray
            })
        }

        let selectedUserStr = localStorage.getItem('selectedUser')
        if (selectedUserStr) {
            let a = JSON.parse(selectedUserStr)
            this.setState({
                selectedUser: a
            })
            this.getMessageHistory(a)
        }

        let messagesStr = localStorage.getItem('messages')
        if (messagesStr) {
            let messagesArray = JSON.parse(messagesStr);
            this.setState({
                messages: messagesArray
            })
        }
    }

    render() {

        let {users, selectedUser, history} = this.state
        return (
            <div className={'container-fluid'}>
                <div className="row">
                    <div className="col-md-3 sider-parent">
                        <Sider users={users} addUser={this.addUser} selectUser={this.selectUser}
                               selectedUser={selectedUser}/>
                    </div>
                    <div className="col-md-9 content-parent">
                        <Content selectedUser={selectedUser} sendMessage={this.sendMessage} history={history}/>
                    </div>
                </div>

            </div>
        );
    }
}

export default App;